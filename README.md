# e3-common conda recipe

Recipe license: BSD 3-Clause

Summary: EPICS e3-common meta package

Includes all e3 modules that are required in all ESS IOCs.

