#!/bin/bash

E3_COMMON_DIR="${PREFIX}/modules/${PKG_NAME}/${PKG_VERSION}"

# Install iocsh script
mkdir -p ${E3_COMMON_DIR}
install -m 644 e3-common.iocsh ${E3_COMMON_DIR}/

# Create activate/deactivate scripts
mkdir -p $PREFIX/etc/conda/activate.d
cat <<EOF > $PREFIX/etc/conda/activate.d/${PKG_NAME}_activate.sh
export E3_COMMON_DIR="${E3_COMMON_DIR}"
EOF

mkdir -p $PREFIX/etc/conda/deactivate.d
cat <<EOF > $PREFIX/etc/conda/deactivate.d/${PKG_NAME}_deactivate.sh
unset E3_COMMON_DIR
EOF
